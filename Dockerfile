FROM openjdk:11-jre-slim-sid
# RUN apt-get update && apt install ffmpeg -y 
EXPOSE 8080
WORKDIR /app
COPY ./archive /app/archive
COPY ./thirdparty /app/thirdparty
COPY ./.env.develop /app/.env.develop
COPY ./build/libs/converter-0.0.1-SNAPSHOT.jar /app/converter-0.0.1-SNAPSHOT.jar
ENTRYPOINT [ "java", "-jar", "converter-0.0.1-SNAPSHOT.jar"]
